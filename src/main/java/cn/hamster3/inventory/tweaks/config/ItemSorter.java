package cn.hamster3.inventory.tweaks.config;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Comparator;

/**
 * 物品排序器
 * 初版只需要通过Material声明顺序排序即可
 */
public class ItemSorter implements Comparator<ItemStack> {
    public static final ItemSorter instance = new ItemSorter();

    private ItemSorter() {
    }

    @Override
    public int compare(ItemStack o1, ItemStack o2) {
        if (o1 == o2) return 0;
        if (o1 == null || o1.getType() == Material.AIR) return 1;
        if (o2 == null || o2.getType() == Material.AIR) return -1;
        return o1.getType().compareTo(o2.getType());
    }
}
